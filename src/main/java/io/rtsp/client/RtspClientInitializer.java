package io.rtsp.client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.rtsp.RtspRequestEncoder;
import io.netty.handler.codec.rtsp.RtspResponseDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author legkunec
 * @since 08.05.14 19:01
 */
public class RtspClientInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline p = socketChannel.pipeline();

        p.addLast("log", new LoggingHandler(LogLevel.INFO));

//        p.addLast("codec", new RtspClientCodec());

        //content decompression
        //p.addLast("inflater", new HttpContentDecompressor());

        p.addLast("decoder", new RtspResponseDecoder());
        p.addLast("encoder", new RtspRequestEncoder());

        p.addLast("handler", new RtspClientHandler());
    }
}
