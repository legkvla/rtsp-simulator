package io.rtsp.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.rtsp.RtspHeaders;
import io.netty.handler.codec.rtsp.RtspMethods;
import io.netty.handler.codec.rtsp.RtspVersions;
import io.netty.util.CharsetUtil;

import java.net.InetSocketAddress;
import java.net.URI;

/**
 * @author legkunec
 * @since 08.05.14 17:43
 */
public class RtspClient {

    private final URI uri;

    private EventLoopGroup group;
    private Channel channel;

    public RtspClient(URI uri) {
        this.uri = uri;
    }

    public void run() throws Exception {
        group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.remoteAddress(new InetSocketAddress(uri.getHost(), uri.getPort()));
            b.group(group).channel(NioSocketChannel.class).handler(new RtspClientInitializer());
            channel = b.connect().sync().channel();
            sendRequest(channel);
            channel.closeFuture().sync();
        } finally {
            group.shutdownGracefully();
        }
    }

    private void sendRequest(Channel ch) {
        // Prepare the HTTP request.
        HttpRequest request;
        if (uri.getQuery() != null && uri.getQuery().length() > 0) {
            request = new DefaultFullHttpRequest(
                    RtspVersions.RTSP_1_0, RtspMethods.PLAY, uri.getRawPath(),
                    Unpooled.copiedBuffer(uri.getQuery(), CharsetUtil.US_ASCII));
        } else {
            request = new DefaultFullHttpRequest(
                    RtspVersions.RTSP_1_0, RtspMethods.PLAY, uri.getRawPath());
        }
        request.headers().set(RtspHeaders.Names.HOST, uri.getHost());
        request.headers().set(RtspHeaders.Names.CONNECTION, RtspHeaders.Values.CLOSE);
//        request.headers().set(HttpHeaders.Names.ACCEPT_ENCODING, HttpHeaders.Values.GZIP);
        // Send the HTTP request.
        ch.writeAndFlush(request);
    }

    public static void main(String[] args) throws Exception {
        RtspClient rtspClient = new RtspClient(new URI("rtsp://localhost:8554/axis-media/media.amp?videocodec=h264&resolution=640x480"));
//        RtspClient rtspClient = new RtspClient(new URI("rtsp://localhost:8554/axis"));
        rtspClient.run();
    }
}
