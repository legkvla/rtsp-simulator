package io.rtsp.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.rtsp.RtspRequestDecoder;
import io.netty.handler.codec.rtsp.RtspResponseEncoder;

/**
 * @author legkunec
 * @since 12.05.14 14:48
 */
public class RtspServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline p = socketChannel.pipeline();
//        p.addLast("log", new LoggingHandler(LogLevel.INFO));
        p.addLast("decoder", new RtspRequestDecoder());
        p.addLast("encoder", new RtspResponseEncoder());
        p.addLast("handler", new RtspServerHandler());
    }
}
